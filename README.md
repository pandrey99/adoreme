# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

## Adore Me Project ##

*V1*

### How do I get set up? ###

* Folowing cmds should be run into console :
	```
	php createDataSource.php
	```
	**creates the Data Source**
	
	``` 
	composer require --dev phpunit/phpunit ^9.5
	```
	**--> install PHPUnit (composer required)**
	
* Dependencies
	**composer (to install PHPUnit)**
	**php >= v7.4**
	**MySQL >= v6.3**
	
* Database configuration
	**DataSource configuration can be found in createDeliveriesTable.sql**
* How to run tests
	```
	"./vendor/bin/phpunit" --testdox DeliveryEstimationUnitTest.php (More on https://phpunit.readthedocs.io/en/9.5/textui.html)
	```
* How to get a delivery date
	```
	php getEstimationScript.php [zip_code] [date_range]
	```