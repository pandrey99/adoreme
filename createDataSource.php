<?php
    include_once (__DIR__."/db_info.php");

    Class DataSource {
        static $_instance;
        private $dataSourceTable = "deliveries";
        static $dataSourceRowsLimit = 5000;
        private $zip_codes = ["041573", "810182", "700123", "400058", "300231", "600005", "500152", "810529", "040852", "042169"];
        private $limitReached = 0;
        protected $holidays = [];
        protected $_DB;
        private $additional_inserts = 0;

        static function instance() {
            if (! isset(self::$_instance) ) {
                // Create a new session instance
                self::$_instance = new self;
            }
            return self::$_instance;
        }

        public function __construct() {
            $this->_DB = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
            $this->zip_code_limit = self::$dataSourceRowsLimit / count($this->zip_codes);
            $this->min_limit_random_date = time() - (365 * 24 * 60 * 60); // current Unix timestamp 1 year ago
                                                                          // used as min limit when generating random dates

            $table_exists = true;
            /** Check data source existence */
            $qry_check = "SHOW TABLES LIKE '$this->dataSourceTable';";
            if ($result = $this->_DB->query($qry_check)) {
                if ($result->num_rows == 0) {
                    $table_exists = false;
                }

                /* free result set */
                $result->close();
            }
            if ($table_exists) {
                // check if limit reached
                $result = $this->_DB->query("SELECT COUNT(id) as no_of_rows FROM `$this->dataSourceTable`;")->fetch_row();
                $curr_no_rows = intval($result[0]);
                if ($curr_no_rows >= self::$dataSourceRowsLimit) {
                    $this->limitReached = 1;
                } else {
                    $this->zip_code_limit = floor((self::$dataSourceRowsLimit - $curr_no_rows) / count($this->zip_codes));
                    $this->additional_inserts =  (self::$dataSourceRowsLimit - $curr_no_rows) % count($this->zip_codes);
                }
            } else {
                $sql = file_get_contents(__DIR__.'/createDeliveriesTable.sql');
                if (! $this->_DB->multi_query($sql)) {
                    exit('Something went wrong');
                }

                while($this->_DB->more_results()){
                    $this->_DB->next_result();
                    $this->_DB->use_result();
                }
            }
        }

        public function generateDataSource() {
            if ($this->limitReached) {
                return;
            }

            $insert_log_qry = "
                INSERT INTO `$this->dataSourceTable` (`zip_code`, `shipment_date`, `delivered_date`) VALUES (?, ?, ?);
            ";
            $additional_inserted = 0;

            foreach ($this->zip_codes as $zip_code) {
                for ($i = 0; $i < $this->zip_code_limit; $i++) {
                    // generate a random timestamp between 1 year ago and current date
                    $random_shipment_timestamp = mt_rand($this->min_limit_random_date, time());

                    // format Unix timestamp into readable date
                    $random_shipment_date = date('Y-m-d H:i:s', $random_shipment_timestamp);

                    // generate a random difference of days between shipment_date and delivered_date in the given interval (3 - 14 days)
                    $diff = mt_rand(3, 14);

                    // add to shipment_date => delivered_date
                    $random_delivered_timestamp = $random_shipment_timestamp + ($diff * 24 * 60 * 60);

                    $start_time = microtime(true);
                    while (($workingDays = $this->getWorkingDays($random_shipment_timestamp, $random_delivered_timestamp, true)) !== $diff) {
                        $random_delivered_timestamp += ($diff - $workingDays) * 24 * 60 * 60;
                    }

                    $random_delivered_date = date('Y-m-d H:i:s', $random_delivered_timestamp);

                    $stmt = $this->_DB->prepare($insert_log_qry);
                    $stmt->bind_param('sss', $zip_code, $random_shipment_date, $random_delivered_date);
                    if(! $stmt->execute()) {
                        print_r($this->_DB->error);
                        exit();
                    }
                    $stmt->close();
                }

                if ($this->additional_inserts > 0 && $additional_inserted < $this->additional_inserts) {
                    // generate a random timestamp between 1 year ago and current date
                    $random_shipment_timestamp = mt_rand($this->min_limit_random_date, time());

                    // format Unix timestamp into readable date
                    $random_shipment_date = date('Y-m-d H:i:s', $random_shipment_timestamp);

                    // generate a random difference of days between shipment_date and delivered_date in the given interval (3 - 14 days)
                    $diff = mt_rand(3, 14);

                    // add to shipment_date => delivered_date
                    $random_delivered_timestamp = $random_shipment_timestamp + ($diff * 24 * 60 * 60);

                    $start_time = microtime(true);
                    while (($workingDays = $this->getWorkingDays($random_shipment_timestamp, $random_delivered_timestamp, true)) !== $diff) {
                        $random_delivered_timestamp += ($diff - $workingDays) * 24 * 60 * 60;
                    }

                    $random_delivered_date = date('Y-m-d H:i:s', $random_delivered_timestamp);

                    $stmt = $this->_DB->prepare($insert_log_qry);
                    $stmt->bind_param('sss', $zip_code, $random_shipment_date, $random_delivered_date);
                    if(! $stmt->execute()) {
                        print_r($this->_DB->error);
                        exit();
                    }
                    $stmt->close();

                    $additional_inserted++;
                }
            }
        }

        public function getWorkingDays($startDate, $endDate, $noConvert=false) {
            if (! $noConvert) {
                $endDate = strtotime($endDate);
                $startDate = strtotime($startDate);
            }

            //The total number of days between the two dates. We compute the no. of seconds and divide it to 60*60*24
            //We add one to inlude both dates in the interval.
            $days = ($endDate - $startDate) / 86400 + 1;

            $no_full_weeks = floor($days / 7);
            $no_remaining_days = fmod($days, 7);

            //It will return 1 if it's Monday,.. ,7 for Sunday
            $the_first_day_of_week = date("N", $startDate);
            $the_last_day_of_week = date("N", $endDate);

            //---->The two can be equal in leap years when february has 29 days, the equal sign is added here
            //In the first case the whole interval is within a week, in the second case the interval falls in two weeks.
            if ($the_first_day_of_week <= $the_last_day_of_week) {
                if ($the_first_day_of_week <= 6 && 6 <= $the_last_day_of_week) $no_remaining_days--;
                if ($the_first_day_of_week <= 7 && 7 <= $the_last_day_of_week) $no_remaining_days--;
            } else {
                // (edit by Tokes to fix an edge case where the start day was a Sunday
                // and the end day was NOT a Saturday)

                // the day of the week for start is later than the day of the week for end
                if ($the_first_day_of_week == 7) {
                    // if the start date is a Sunday, then we definitely subtract 1 day
                    $no_remaining_days--;

                    if ($the_last_day_of_week == 6) {
                        // if the end date is a Saturday, then we subtract another day
                        $no_remaining_days--;
                    }
                } else {
                    // the start date was a Saturday (or earlier), and the end date was (Mon..Fri)
                    // so we skip an entire weekend and subtract 2 days
                    $no_remaining_days -= 2;
                }
            }

           //The no. of business days is: (number of weeks between the two dates) * (5 working days) + the remainder
           //---->february in none leap years gave a remainder of 0 but still calculated weekends between first and last day, this is one way to fix it
           $workingDays = $no_full_weeks * 5;
           if ($no_remaining_days > 0 ) {
               $workingDays += $no_remaining_days;
           }

            //We subtract the holidays
            foreach($this->holidays as $holiday) {
                $time_stamp = strtotime($holiday);
                //If the holiday doesn't fall in weekend
                if ($startDate <= $time_stamp && $time_stamp <= $endDate && date("N", $time_stamp) != 6 && date("N", $time_stamp) != 7)
                    $workingDays--;
            }

            return intval($workingDays);
        }
    }

    DataSource::instance()->generateDataSource();
