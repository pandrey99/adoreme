<?php
    include_once (__DIR__."/createDataSource.php");

    Class Delivery {
        static $_instance;
        protected $_DB;
        private $_deliveries_table = "deliveries";
        private $_mysql_working_days_function_declaration_file = "createTotalWorkingDaysMySQLFunction.sql";
        private $_months = ["january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december"];

        static function instance() {
            if (! isset(self::$_instance) ) {
                // Create a new session instance
                self::$_instance = new self;
            }
            return self::$_instance;
        }

        public function __construct() {
            $this->_DB = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

            if (! $this->_DB) {
                echo json_encode(["status" => "error", "msg" => "Server Error"]);
                exit();
            }

            /** Check data source existence */
            $qry_check = "SHOW TABLES LIKE '$this->_deliveries_table';";
            if ($result = $this->_DB->query($qry_check)) {
                if ($result->num_rows == 0) {
                    throw new \Exception('DataSource missing!', 100);
                }

                /* free result set */
                $result->close();
            }

            $sql = file_get_contents(__DIR__.'/'.$this->_mysql_working_days_function_declaration_file);
            if (! $this->_DB->multi_query($sql)) {
                exit('Something went wrong');
            }

            while($this->_DB->more_results()){
                $this->_DB->next_result();
                $this->_DB->use_result();
            }
        }

        private function checkZipCodeExistence($zip_code) {
            $check_qry = "
                SELECT COUNT(id) <> 0 AS 'zip_code_exists'
                FROM deliveries
                WHERE zip_code = ?;
            ";
            $stmt = $this->_DB->prepare($check_qry);
            if (is_bool($stmt)) {
                var_dump($this->_DB->error);
                die();
            }
            $stmt->bind_param('s', $zip_code);
            if (! $stmt->execute()) {
                return false;
            }
            $result = $stmt->get_result()->fetch_row();
            $check = isset($result[0]) ? $result[0] : false;

            return (bool) $check;
        }

        public function getEstimation($params=null) {
            if (! isset($params)) {
                echo json_encode([ 'status' => 'error', 'msg' => 'Input required' ]);
                return;
            }
            if (! isset($params['zip_code'])) {
                echo json_encode([ 'status' => 'error', 'msg' => 'Zip code required' ]);
                return;
            } else {
                if (! $this->checkZipCodeExistence($params['zip_code'])) {
                    echo json_encode([ 'status' => 'error', 'msg' => 'Zip code not found' ]);
                    return;
                }
            }

            $bind_string = 's';
            $bindings = [ $params['zip_code'] ];
            $where = "WHERE zip_code = ? ";

            if (! isset($params['date_range'])) {
                $date_range = 'all_time';
            } else {
                $start_date = $end_date = null;
                $date_range = trim($params['date_range']);

                if (in_array(strtolower($date_range), $this->_months)) {
                    $start_date = date('Y') . '-' . date('m', strtotime($date_range)) . '-' . '01';
                    $end_date = date('Y') . '-' . date('m', strtotime($date_range)) . '-' . '31';
                } else {
                    $strtotime_compatibility = strtotime($date_range);
                    if ($strtotime_compatibility === FALSE) {
                        if (strpos($date_range, "current date") === 0) {
                            $diff = explode("current date", $date_range)[1];
                            $diff = preg_replace("/(\s)*ago/", "", $diff);

                            $start_date = date('Y-m-d', strtotime($diff));
                        } else if (preg_match("/^last\s*\d*(\s)*(hour(s)?|day(s)?|month(s)?|year(s)?)\s*$/", $date_range, $matches) === 1) {
                            $start_date = str_replace('last', '-', $matches[0]);
                            $start_date = date('Y-m-d', strtotime($start_date));
                        } else if (strpos($date_range, 'between') === 0) {
                            $date_range_copy = str_replace(['between', '-', 'and'], ['', '.', ' - '], $date_range);

                            $dates = array_map('trim', explode('-', $date_range_copy));
                            $start_date = strtotime($dates[0]) === FALSE ? null : date('Y-m-d', strtotime($dates[0]));
                            $end_date = strtotime($dates[1]) === FALSE ? null : date('Y-m-d', strtotime($dates[1]));

                            if (count($dates) != 2 || ! isset($start_date) || ! isset($end_date)) {
                                echo json_encode([ 'status' => 'error', 'msg' => 'Unknown date format!' ]);
                                return;
                            }
                        } else if (preg_match_all('/\\d{2,4}(-|\\.|\\s)\\d{2,4}(-|\\.|\\s)\\d{2,4}/', $date_range, $matches) != 0) {
                            $dates = array_map('trim', $matches[0]);
                            $start_date = strtotime($dates[0]) === FALSE ? null : date('Y-m-d', strtotime($dates[0]));
                            $end_date = strtotime($dates[1]) === FALSE ? null : date('Y-m-d', strtotime($dates[1]));

                            if (count($dates) != 2 || ! isset($start_date) || ! isset($end_date)) {
                                echo json_encode([ 'status' => 'error', 'msg' => 'Unknown date format!' ]);
                                return;
                            }
                        } else {
                            echo json_encode([ 'status' => 'error', 'msg' => 'Unknown date format!' ]);
                            return;
                        }
                    } else if (date('Y-m-d', $strtotime_compatibility) !== date('Y-m-d', time())) {
                        $start_date = $strtotime_compatibility;
                    } else {
                        echo json_encode([ 'status' => 'error', 'msg' => 'Could not retrieve data!' ]);
                        return;
                    }
                }

                if (isset($start_date)) {
                    $bind_string .= 's';
                    $bindings[] = $start_date;
                    $where .= " AND DATE_FORMAT(shipment_date, '%Y-%m-%d') >= ? ";
                } else {
                    echo json_encode([ 'status' => 'error', 'msg' => 'Could not retrieve data!' ]);
                    return;
                }
                if (isset($end_date)) {
                    $bind_string .= 's';
                    $bindings[] = $end_date;
                    $where .= " AND DATE_FORMAT(shipment_date, '%Y-%m-%d') <= ? ";
                }
            }

            $estimation_select_qry = "
                SELECT MIN(delivery_working_days) AS min_working_days, MAX(delivery_working_days) AS max_working_days
                FROM (
                	SELECT zip_code, TOTAL_WORKINGDAYS(delivered_date, shipment_date) AS delivery_working_days
                	FROM deliveries
                	$where
                	ORDER BY 2, shipment_date
                ) T;
            ";

            $stmt = $this->_DB->prepare($estimation_select_qry);
            $stmt->bind_param($bind_string, ...$bindings);
            if (! $stmt->execute()) {
                echo json_encode([ 'status' => 'error', 'msg' => 'Could not retrieve data!' ]);
                return;
            }
            $result = $stmt->get_result()->fetch_assoc();

            if (! isset($result['min_working_days']) || ! isset($result['max_working_days'])) {
                echo json_encode([ 'status' => 'error', 'msg' => 'Not enough history!' ]);
                return;
            }

            $min_no_of_working_days_needed = intval($result['min_working_days']);
            $max_no_of_working_days_needed = intval($result['max_working_days']);

            if (! is_int($min_no_of_working_days_needed) || ! isset($min_no_of_working_days_needed) ||
                ! is_int($max_no_of_working_days_needed) || ! isset($max_no_of_working_days_needed) ) {
                echo json_encode([ 'status' => 'error', 'msg' => 'Could not retrieve data!' ]);
                return;
            }

            $min_estimated_date = date('Y-m-d', (time() + $min_no_of_working_days_needed * 24 * 60 * 60));
            $max_estimated_date = date('Y-m-d', (time() + $max_no_of_working_days_needed * 24 * 60 * 60));

            if ($min_no_of_working_days_needed == $max_no_of_working_days_needed) {
                $ret_string = $min_estimated_date . ' (' . $min_no_of_working_days_needed . ' working days)';
            } else {
                $ret_string = $min_estimated_date . ' - ' . $max_estimated_date . ' (' .
                                $min_no_of_working_days_needed . ' - ' . $max_no_of_working_days_needed . ' working days)';
            }

            echo json_encode(['status' => 'success', 'estimated_date' => $ret_string]);
            return;
        }

        public function getEstimatedDate($params=null) {
            try {
                return $this->getEstimation($params);
            } catch (Exception $e) {
                echo json_encode([ 'status' => 'error', 'msg' => 'Could not retrieve data!' ]);
                return;
            }
        }
    }

?>