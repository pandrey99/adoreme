<?php declare(strict_types=1);

    include __DIR__."/vendor/autoload.php";
    include_once (__DIR__."/Delivery.php");
    include_once (__DIR__."/DataBase.php");

    use PHPUnit\Framework\TestCase;

    final class DeliveryEstimationUnitTest extends TestCase {
        public function testNoInput() {
            $expected_output = json_encode([ 'status' => 'error', 'msg' => 'Input required' ]);
            $this->expectOutputString($expected_output);

            Delivery::instance()->getEstimatedDate();
        }

        public function testNoZipCodeProvided() {
            $expected_output = json_encode([ 'status' => 'error', 'msg' => 'Zip code required' ]);
            $this->expectOutputString($expected_output);

            Delivery::instance()->getEstimatedDate(['date_range' => 'between February and June']);
        }

        public function testInvalidZipCode() {
            $expected_output = json_encode([ 'status' => 'error', 'msg' => 'Zip code not found' ]);
            $this->expectOutputString($expected_output);

            Delivery::instance()->getEstimatedDate(['zip_code' => '2020-01-01']);
        }

        public function testUnknownDateFormat() {
            $expected_output = json_encode([ 'status' => 'error', 'msg' => 'Unknown date format!' ]);
            $this->expectOutputString($expected_output);

            Delivery::instance()->getEstimatedDate(['zip_code' => '041573', 'date_range' => 'between February and June and between March and April']);
        }

        public function testNotEnoughLogs() {
            $expected_output = json_encode([ 'status' => 'error', 'msg' => 'Not enough history!' ]);
            $this->expectOutputString($expected_output);

            Delivery::instance()->getEstimatedDate(['zip_code' => '300231', 'date_range' => '2019-01-02 - 2019-12-01']);
        }

        public function testGetEstimationDate() {
            $expected_output = '{"status":"success","estimated_date":"2021-02-12 - 2021-02-18 (4 - 10 working days)"}';
            $this->expectOutputString($expected_output);

            Delivery::instance()->getEstimatedDate(['zip_code' => '810529', 'date_range' => '2020-02-20 00:01:00 - 2020-02-20 07:00:00']);
        }

        /**
        *      @expectedException DataSource missing!
        */
        public function testNonExistentDataSource() {
            if (DataBaseConnector::instance()->executeQuery("DROP TABLE deliveries;")) {
                try {
                    $this->expectException("Exception");
                    $this->expectExceptionCode(100);
                    $this->expectExceptionMessage("DataSource missing!");
                    $this->expectExceptionMessageRegExp('/missing/');

                    Delivery::instance()->getEstimatedDate(['zip_code' => '810529', 'date_range' => '2020-02-20 00:01:00 - 2020-02-20 07:00:00']);
                } catch (Exception $e) { }
            }
        }
    }
