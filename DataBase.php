<?php

    include_once (__DIR__."/db_info.php");

    Class DataBaseConnector {
        static $_instance;
        static $staticConnection = null;
        protected $connection;

        static function instance() {
            if (! isset(self::$_instance) ) {
                // Create a new session instance
                self::$_instance = new self;
            }
            return self::$_instance;
        }

        public function __construct() {
            if (self::$staticConnection == null) {
                self::$staticConnection = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
            }
            $this->connection = self::$staticConnection;
        }

        public function executeQuery($query) {
            return $this->connection->query($query);
        }
    }