<?php

    include_once(__DIR__.'/Delivery.php');

    if (isset($argv[1])) {
        if (isset($argv[2])) {
            Delivery::instance()->getEstimation(['zip_code' => $argv[1], 'date_range' => $argv[2]]);
        } else {
            Delivery::instance()->getEstimation(['zip_code' => $argv[1]]);
        }
    } else {
        Delivery::instance()->getEstimation();
    }

?>