CREATE TABLE `deliveries` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`zip_code` VARCHAR(50) NOT NULL,
	`shipment_date` TIMESTAMP NOT NULL DEFAULT current_timestamp(),
	`delivered_date` TIMESTAMP NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	INDEX `zip_code` (`zip_code`)
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
ROW_FORMAT=DYNAMIC
AUTO_INCREMENT=1
;